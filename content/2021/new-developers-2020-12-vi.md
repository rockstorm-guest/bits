Title: Những Bảo trì Debian mới (Tháng mười một và mười hai 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Timo Röhling
  * Fabio Augusto De Muzio Tobich
  * Arun Kumar Pariyar
  * Francis Murtagh
  * William Desportes
  * Robin Gustafsson
  * Nicholas Guriev

Xin chúc mừng!

