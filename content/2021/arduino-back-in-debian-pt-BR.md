Title: Arduino está de volta ao Debian
Slug: arduino-back-in-debian
Date: 2021-02-01 08:00
Author: Rock Storm
Tags: arduino, announce
Lang: pt-BR
Translator: Carlos Henrique Lima Melara, Thiago Pezzo (Tico)
Status: published

O time Debian Eletrônica está feliz em anunciar que a versão mais recente
do Arduino, provavelmente a mais disseminada plataforma de programação de
microcontroladores AVR, está agora [empacotada e disponível no Debian instável
(unstable)][1].

A última versão do Arduino que estava disponível no Debian era a 1.0.5, a qual
foi lançada em 2013. Foram anos de tentativas e falhas, mas finalmente, após
um grande esforço de vários meses de Carsten Schoenert e Rock Storm, nós temos
um pacote funcional da mais recente versão do Arduino. Após 7 anos, será
possível instalar a IDE Arduino simplesmente digitando *"apt install arduino"*
novamente.

*"O propósito desta publicação não é somente anunciar a disponibilidade do
pacote, mas também solicitar, na verdade, que ele seja testado"* disse Rock
Storm. *" O título poderia muito bem ter sido _Procura-se: beta testers para
Arduino_ (mortos ou vivos :P)."*. O time de eletrônicos do Debian apreciaria
se pessoas com as ferramentas e o conhecimento necessário pudessem testar o
pacote e informar se foram encontrados quaisquer problemas.

Com esta publicação, agradecemos o time de eletrônicos do Debian e todos(as)
os(as) contribuidores(as) do pacote. Este feito não seria possível sem a
contribuição de vocês.

[1]: https://packages.debian.org/unstable/arduino
