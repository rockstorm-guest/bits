Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2017)
Slug: new-developers-2017-02
Date: 2017-03-08 00:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

  * Ulrike Uhlig (ulrike)
  * Hanno Wagner (wagner)
  * Jose M Calhariz (calharis)
  * Bastien Roucariès (rouca)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

  * Dara Adib
  * Félix Sipma
  * Kunal Mehta
  * Valentin Vidic
  * Adrian Alves
  * William Blough
  * Jan Luca Naumann
  * Mohanasundaram Devarajulu
  * Paulo Henrique de Lima Santana
  * Vincent Prat

Félicitations !
