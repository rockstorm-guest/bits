Title: DebConf16 starts in Cape Town
Slug: debconf16-starts
Date: 2016-06-25 09:00
Author: Laura Arjona Reina
Tags: debconf, debconf16, debian
Status: draft

DebConf is the annual conference for Debian contributors and users interested in improving [the Debian Project](https://www.debian.org), one of the largest Free Software projects worldwide. This year [DebConf16](https://debconf16.debconf.org/) will be hosted in Cape Town, South Africa, for the first time.

Starting from 23 June with the [DebCamp Sprints](https://debconf16.debconf.org/debcamp-sprints/), the volunteers who create this operating system will gather in the University of Cape Town to concentrate on the development and improvement of the Debian distribution.

DebConf proper starts the weekend of 2 and 3 July with the [Open Festival](https://debconf16.debconf.org/open-festival/), where events of interest to a wider audience are offered, ranging from topics specific to Debian to a wider appreciation of the open and maker movements (and not just IT-related). Hackers, makers, hobbyists and other interested parties are invited to share their activities with DebConf attendees and the public at the University of Cape Town, whether in form of workshops, lightning talks, install parties, art exhibition or posters. Additionally, a Job Fair will take place on Saturday, and its job wall will be available throughout DebConf.

The full schedule of the Debian Conference thorough the week is [published](https://debconf16.debconf.org/schedule/). After the Open Festival, the conference will continue with more than 85 [talks and BoFs](https://debconf16.debconf.org/talks/) (informal gatherings and discussions within Debian teams), including not only software development and packaging but also areas like translation, documentation, artwork, testing, specialized derivatives, maintenance of the community infrastructure, and other.

[Registrations](https://debconf16.debconf.org/about/registration/) for attendance to DebConf16 are still accepted in any of the basic, professional, and corporate categories. Thanks to the commitment of the [DebConf16 sponsors](https://debconf16.debconf.org/sponsors/), including Hewlett Packard Enterprise as Platinum Sponsor and numerous local and international companies and organizations, basic registration for DebConf is free of charge for attendees.
There will be live streaming of the event on [https://debconf16.debconf.org](https://debconf16.debconf.org) and talks will be recorded and stored in a video archive for further comsumption.

DebConf is committed to a safe and welcome environment for all participants. See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.