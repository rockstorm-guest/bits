Title: DPL elections 2016, congratulations Mehdi Dogguy!
Date: 2016-04-17 18:40
Tags: dpl
Slug: results-dpl-elections-2016
Author: Ana Guerrero Lopez
Status: published

The Debian Project Leader elections finished yesterday and the winner is Mehdi Dogguy!
Of a total of 1023 developers, 282 developers voted using the [Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the result is available in the [Debian Project Leader Elections 2016 page](https://www.debian.org/vote/2016/vote_001).

The new term for the project leader starts today April 17th and expire on April 17th 2017.
