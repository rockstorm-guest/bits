Title: Elecciones a Líder del Proyecto Debian 2015
Date: 2015-03-12 11:00
Tags: dpl, vote
Slug: dpl-elections-2015
Author: Ana Guerrero Lopez
Translator: Daniel Cialdella
Lang: es
Status: published

Ha llegado de nuevo ese momento del año para el proyecto Debian:
[las elecciones para Líder del Proyecto (DPL)](https://www.debian.org/vote/2015/vote_001)!
Comenzando el 1 de abril y durante las dos semanas siguientes, los desarrolladores
de Debian votarán para elegir la persona que guiará el proyecto por un año.
Los resultados serán publicados el 15 de abril y el nuevo período para el
nuevo líder comenzará el 17 de abril del 2015.

Lucas Nussbaum, quien ha estado a cargo de la oficina por los últimos dos años,
no buscará su reelección este año y los Desarrolladores Debian deberán elegir
entre tres candidatos:

* [Mehdi Dogguy](https://www.debian.org/vote/2015/platforms/mehdi)
* [Gergely Nagy](https://www.debian.org/vote/2015/platforms/algernon)
* [Neil McGovern](https://www.debian.org/vote/2015/platforms/neilm)

Gergely Nagy y Neil McGovern fueron candidatos a DPL en años anteriores;
es la primera vez para Mehdi Dogguy.

El período de Campaña comenzó hoy y continuará hasta el 31 de marzo. Se espera que los
candidatos participen en debates y discusiones en la [Lista de distribución
debian-vote](http://lists.debian.org/debian-vote/) donde responderán a las preguntas
de usuarios y contribuidores.

