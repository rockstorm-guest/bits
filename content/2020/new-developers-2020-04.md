Title: New Debian Developers and Maintainers (March and April 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator: 
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Paride Legovini (paride)
  * Ana Custura (acute)
  * Felix Lechner (lechner)

The following contributors were added as Debian Maintainers in the last two months:

  * Sven Geuer
  * Håvard Flaget Aasen

Congratulations!

