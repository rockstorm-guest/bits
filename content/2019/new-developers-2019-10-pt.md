Title: Novos desenvolvedores e mantenedores Debian (setembro e outubro de 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos últimos dois meses:

  * Teus Benschop (teusbenschop)
  * Nick Morrott (nickm)
  * Ondřej Kobližek (kobla)
  * Clément Hermann (nodens)
  * Gordon Ball (chronitis)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos últimos dois meses:

  * Nikos Tsipinakis
  * Joan Lledó
  * Baptiste Beauplat
  * Jianfeng Li

Parabéns a todos!

