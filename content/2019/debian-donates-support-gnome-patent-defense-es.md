Title: Debian dona en apoyo a GNOME en su defensa sobre patentes
Slug: debian-donates-support-gnome-patent-defense
Date: 2019-10-28 17:00
Author: Sam Hartman
Tags: debian, gnome, patent trolls, fundraising, donation
Status: published
Lang: es
Translator: Laura Arjona Reina

Hoy el proyecto Debian se compromete a [donar] 5.000 dólares estadounidenses
a la fundación GNOME en apoyo para su defensa sobre patentes. El 23 de octubre
[escribimos para expresar nuestro apoyo a GNOME] en un asunto que afecta a toda
la comunidad de software libre. Hoy hacemos ese apoyo tangible.

"*Esto es más grande que GNOME,*" dijo el Líder del Proyecto Debian, Sam Hartman.
"*Al plantarnos juntos y demostrar que la comunidad de software libre en su conjunto
está con GNOME, podemos enviar un mensaje fuerte a las «entidades no practicantes»
(troles de patentes). Cuando ponen en su objetivo alguien de la comunidad de software
libre, nos están poniendo a todos como objetivo. Lucharemos, y lucharemos para
invalidar su patente. Para nosotros, esto es más que dinero.
Esto es sobre nuestra libertad para construir y distribuir nuestro software.*"

"*Estamos increíblemente agradecidos a Debian por este tipo de donación,
y también por su apoyo,*" dijo Neil McGovern, Director Ejecutivo de la fundación
GNOME. "*Es alentador ver que cuando el software libre está siendo atacado de esta
manera todos nos aliamos en un frente unido.*"

Si GNOME necesita más dinero más adelante en su defensa, Debian estará ahí para
apoyar a la fundación GNOME. Animamos a personas individuales y organizaciones
a unirse a nosotros y tomar una posición firme contra los troles de patentes.

[escribimos para expresar nuestro apoyo a GNOME]: https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html
[donar]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
