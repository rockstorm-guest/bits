Title: Debian fa una donazione per la difesa di GNOME
Slug: debian-donates-support-gnome-patent-defense
Date: 2019-10-28 17:00
Author: Sam Hartman
Tags: debian, gnome, patent trolls, fundraising, donation
Status: published
Lang: it
Translator: Sebastiano Pistore

Oggi il Progetto Debian si è pubblicamente impegnato a [donare] $ 5,000 alla
Fondazione GNOME per le spese legali che sta sostenendo a causa di un patent troll. Il
23 ottobre abbiamo [reso pubblico il nostro sostegno a GNOME] riguardo ad una minaccia che coinvolge
l'intera comunità del Software Libero: oggi rendiamo concreta tale dichiarazione.

"*Questo problema non è solo di GNOME,*" ha dichiarato Sam Hartman, Leader del Progetto Debian.
"*Riunendo assieme e mostrando pubblicamente che tutta la comunità del Software Libero è alla base
di GNOME, intendiamo mandare un messaggio chiaro
alle entità non praticanti (patent troll). Quando prendi di mira un qualunque membro della comunità del Software
Libero, prendi di mira tutti i membri della comunità. Tutti noi siamo minacciati dalla tua azione legale, e
quindi ci impegneremo per non far valere la tua richiesta. Non è un problema di soldi.
Noi abbiamo la libertà di scrivere e distribuire i nostri software.*"

"*Siamo grati a Debian per la sua donazione e
per il suo supporto,*" ha dichiarato Neil McGovern, Direttore esecutivo della Fondazione
GNOME. "*È stato incoraggiante vedere che quando il
Software Libero viene attaccato in questi modi ignobili siamo in grado di superare le nostre differenze e
di riunirci tutti per fare un unico fronte.*"

Qualora in futuro GNOME avesse bisogno di altri fondi per sostenere questa controversia, Debian sarà pronto
per aiutare la Fondazione GNOME. Incoraggiamo singole persone e
organizzazioni ad unirsi a noi per resistere ai patent troll.

[reso pubblico il nostro sostegno a GNOME]: https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll-it.html
[donare]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
